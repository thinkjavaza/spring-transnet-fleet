package za.co.binarylabs.fleet.service;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by prisca on 2019/10/10.
 */
@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Car not found")
public class CarNotFoundException extends RuntimeException{

    public CarNotFoundException() {
    }

    public CarNotFoundException(String message) {
        super(message);
    }


}
