package za.co.binarylabs.fleet.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;
import za.co.binarylabs.fleet.model.Car;


/**
 * Created by prisca on 2019/10/09.
 */
//@Repository
@RepositoryRestResource(collectionResourceRel = "/cars")
public interface CarRepository extends JpaRepository<Car, String> {



}
