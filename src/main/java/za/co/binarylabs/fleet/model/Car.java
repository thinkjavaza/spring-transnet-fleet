package za.co.binarylabs.fleet.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * Created by prisca on 2019/10/09.
 */
@Entity
public class Car {

    @Id
    private String assetId;

    @JsonFormat(pattern = "dd/MM/yyyy")
    private LocalDate numDate;

    private int speed;

    private int roadSpeed;

    private Long kilometerTravelled;

    private int fuelUsed;

    private int ignition;

    private double longitude;

    private double latitude;

    @JsonFormat(pattern = "dd/MM/yyyy")
    private LocalDate positionDate;

    @JsonFormat(pattern = "dd/MM/yyyy")
    private LocalDateTime positionDateTime;

    @JsonFormat(pattern = "dd/MM/yyyy")
    private LocalDateTime positionRoundedTime;

    private String EventDescription;

    private String DriverSurname;

    private String Tag;

    private String DriverIdNumber;

    private String DriverSAPNumber;

    private int Geo_Fence_In_Out;


    public Car(String id) {
        this.assetId = id;
    }

    public String getId() {
        return assetId;
    }

    public void setId(String id) {
        this.assetId = id;
    }
}
